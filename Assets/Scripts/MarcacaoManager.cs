﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarcacaoManager : MonoBehaviour {

	public Material dirty;
	public Material clean;

	Rigidbody rb;
	CapsuleCollider boomRadius;

	public bool expanding = false;

	void Start(){
		rb = GetComponent<Rigidbody> ();
		boomRadius = GetComponent<CapsuleCollider> ();
	}

	void FixedUpdate(){
		if(expanding){
			boomRadius.radius *= 1.01f;
		}
	}

	public void explode(){
		//cria um circulo que aumenta de tamanho e muda o material de tudo o que ele toca.
		expanding = true;
	}

	public void marcaObjeto(GameObject obj){
		obj.GetComponent<Renderer> ().material = clean;
	}

	void OnTriggerEnter(Collider other){
		marcaObjeto (other.gameObject);
	}

}
