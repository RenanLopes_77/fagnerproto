﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeixeMarcador : MonoBehaviour {
	MarcacaoManager marcacao;

	void Start(){
		marcacao = GameObject.FindObjectOfType<MarcacaoManager> ();
	}

	void OnTriggerEnter(Collider other){
		if (!other.transform.CompareTag ("Peixe")) {
			marcacao.marcaObjeto (other.gameObject);
		}
	}
}
