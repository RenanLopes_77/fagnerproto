﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
	public float standardAnimationTime = 5;

	float animationTime;
	float animationDeltatime;
	Transform target;
	Vector3 startPosition;
	Vector3 startRotation;

	bool moving;
	bool done;

	void FixedUpdate(){
		if(moving){
			animationDeltatime += Time.deltaTime / animationTime;
			if(animationDeltatime > 1f){
				animationDeltatime = 1f;
			}
			transform.position = Vector3.Lerp (startPosition, target.position, animationDeltatime );
			transform.rotation = Quaternion.Euler(Vector3.Lerp (startRotation, target.rotation.eulerAngles, animationDeltatime ));

			if(Vector3.Distance(transform.position, target.position) < 0.1f){
				transform.position = target.position;
				transform.rotation = target.rotation;
				moving = false;
				done = true;
			}
		}
	}

	public void Move(Transform t){
		target = t;
		moving = true;
		animationTime = standardAnimationTime;
		animationDeltatime = 0;
		startPosition = transform.position;
		startRotation = transform.rotation.eulerAngles;
	}

	public void Move(Transform t, float spd){
		target = t;
		moving = true;
		animationTime = spd;
		animationDeltatime = 0;
		startPosition = transform.position;
		startRotation = transform.rotation.eulerAngles;
	}

	public bool isDone(){
		if(done){
			done = false;
			return true;
		}
		return false;
	}
}
