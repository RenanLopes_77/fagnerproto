﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo2Movement : MonoBehaviour {

	int mouseMultiplier = 2;
	public bool mouseEnabled = true;
	public float speed = 3f;
	public GameObject aimTarget;
	public GameObject aimMovement;

	float xR = 0; 
	float yR = 0;

	float hAxis, vAxis;

	void FixedUpdate () {
		GetInput ();
		Rotate ();
		Move ();
	}

	void GetInput(){


		if (mouseEnabled){
			hAxis = (Input.GetAxis("Mouse X")) * mouseMultiplier;
			vAxis = (Input.GetAxis("Mouse Y")) * -mouseMultiplier;
			//transform.Rotate( new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * speed);
		}else{
			hAxis = Input.GetAxis ("Horizontal");
			vAxis = (Input.GetAxis ("Vertical")) * -1;
		}

	}

	void Rotate (){
		
		
		xR += vAxis;
		yR += hAxis; 

		transform.rotation = Quaternion.Euler(xR, yR, 0);
		transform.LookAt (aimTarget.transform);


	}

	void Move(){
			
			transform.localPosition = transform.localPosition + transform.forward * Time.deltaTime * speed;		
	}
}
