﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour {

	// fade in
	// show scene
	// text fade in
	// camera slides to right
	// stops at fish
	// fish is active

	public CameraControl cameraControl;
	public Demo2Movement fish;
	public Transform mainCameraPosition;

	string gamestate = "stage0";

	bool movingCamera = false;

	void MoveCameraToMainPosition(){
		cameraControl.Move (mainCameraPosition);
		movingCamera = true;
	}

	void Update(){
		if(gamestate.Equals ("stage0")){
			if(movingCamera){
				movingCamera = !cameraControl.isDone ();
				if(!movingCamera){
					gamestate = "gameplay";
					fish.enabled = true;
				}
			} else {
				if(Input.GetKeyDown(KeyCode.Space)){
					MoveCameraToMainPosition ();
				}
			}
		}
	}
}
